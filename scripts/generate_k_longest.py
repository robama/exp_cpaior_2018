from random import randint, choice

DZN_PATH = '/home/roberto/exp_cpaior_2018/dzn/k_longest/k_longest_'
SMT_PATH = '/home/roberto/exp_cpaior_2018/smt/k_longest/k_longest_'

# Remove backslash and quotes symbols from printable ASCII characters.
chars = [chr(i) for i in range(32, 126) if i not in [34, 39, 92]]
def randchar():
  return chars[randint(0, len(chars) - 1)]

def output_dzn(L, M, W):
  N = randint(M/2, M - 1);
  for lex in ['lex', 'nq']:
    with open(DZN_PATH + str(L) + '_' + str(M) + '_' + lex + '.dzn', 'w') as f:
      f.write("W = [\n")
      for w in W:
        f.write('"' + w + '",\n')
      if (lex == 'lex'):
        f.write("];\nN = " + str(N) + ";\nLEX = true;\n")
      else:
        f.write("];\nN = " + str(N) + ";\nLEX = false;\n")

for L in [100, 250, 500, 750, 1000]:
  for N in [5, 10, 15, 20]:
    continue #FIXME
    w = reduce(lambda x, y: x + y, [randchar() for _ in range(0, L)])
    indexes = [0, 0]
    while (len(indexes) > len(set(indexes))):
      indexes = sorted([randint(1, L - 1) for _ in range(0, N - 1)])
    strings = [w[0 : indexes[0]]]
    indexes = indexes + [L]
    strings += [w[indexes[i] : indexes[i + 1]] for i in range(0, N - 1)]
    assert len(strings) == N and sum(len(s) for s in strings) == L
    for i in range(0, N - 1):
      for j in range(i + 1, N):
        if randint(0, 1):
          # Force an overlap between string[i] and string[j].
          choice = randint(0, 2)
          li = len(strings[i])
          lj = len(strings[j])
          assert li > 0 and lj > 0
          if (choice == 0):
            if li < lj:
              if strings[j].find(strings[i]) == -1:
                # sub(i, j)
                n = randint(0, lj - li)
                strings[j] = strings[j][:n] + strings[i] + strings[j][n + li:]
            elif (strings[i].find(strings[j]) == -1):
              # sub(j, i)
              n = randint(0, li - lj)
              strings[i] = strings[i][:n] + strings[j] + strings[i][n + lj:]
          elif (choice == 1):
            # i before j
            n = randint(1, min(li, lj))
            strings[j] = strings[i][-n:] + strings[j][n:]
          else:
            # j before i
            n = randint(1, min(li, lj))
            strings[j] = strings[i][-n:] + strings[j][n:]
    assert len(strings) == N and sum(len(s) for s in strings) == L
    # Output to dzn.
    output_dzn(L, N, strings)
    
for L in [100, 250, 500, 750, 1000]:
  for M in [5, 10, 15, 20]:
    W = []
    with open(DZN_PATH + str(L) + '_' + str(M) + '_lex.dzn', 'r') as f:
      for line in f:
        if line.startswith('N = '):
          N = int(line[4:-2])
        elif line.startswith('W = '):
          op = True
        elif line.startswith(']'):
          op = False
        elif op:
          W += [line[1:-3]]
      assert M / 2 <= N <= M - 1 and sum(len(w) for w in W) == L
    M = len(W)
    K = M / 2
    with open(SMT_PATH + str(L) + '_' + str(M) + '.smt2', 'w') as f:
      f.write('(declare-fun L () Int)\n(declare-fun N () Int)\n')
      for i in range(0, K):
        f.write('(declare-fun selected_' + str(i) + ' () String)\n')
        for j in range(0, M):
          f.write('(declare-fun count_' + str(i) + '_' + str(j) + ' () Int)\n')
      f.write('(assert (= N ' + str(N) + '))\n')
      l = '(assert (= L (+'
      for i in range(0, K):
        si = 'selected_' + str(i)
        s = '(assert (<= N (+ '
        l += '(str.len ' + si + ')'
        for j in range(0, M):
          idx = str(i) + '_' + str(j)
          f.write(
            '(assert (ite (str.contains "' + W[j] + '" ' + si +')'\
            '(= count_' + idx + ' 1)(= count_' + idx + ' 0)))\n'
          )
          s += 'count_' + idx + ' '
        for k in range(i + 1, K):
          sk = 'selected_' + str(k)
          f.write(
            '(assert (ite (= ' + si + ' ' + sk + ')(= ' + si + ' "")'\
            ' (<= (str.len ' + si + ')(str.len ' + sk + '))))\n'
          )
        f.write(s + ')))\n')
      f.write(l + ')))\n')
      f.write('(check-sat)\n(get-model)\n')

