from random import randint, choice

DZN_PATH = '/home/roberto/exp_cpaior_2018/dzn/min_super/min_super_'
SMT_PATH = '/home/roberto/exp_cpaior_2018/smt/min_super/min_super_'

# Remove backslash and quotes symbols from printable ASCII characters.
chars = [chr(i) for i in range(32, 126) if i not in [34, 39, 92]]
def randchar():
  return chars[randint(0, len(chars) - 1)]

def output_dzn(L, N, W):
  return
  # Output to dzn:
  with open(DZN_PATH + str(L) + '_' + str(N) + '.dzn', 'w') as f:
    f.write("L = " + str(L) + ";\nN = " + str(N) + ";\n")
    f.write("W = [\n")
    for w in W:
      f.write('"' + w + '",\n')
    f.write("]")
  
def output_smt(L, N, W):
  return
  # Output to smtlib:
  with open(SMT_PATH + str(L) + '_' + str(N) + '.smt2', 'w') as f:
    f.write('(declare-fun L () Int)\n(declare-fun W () String)\n')
    for i in range(0, N):
      f.write('(declare-fun i' + str(i) + ' () Int)\n')
    f.write('(assert (= L (str.len W)))\n')
    f.write('(assert (<= L ' + str(L) + '))\n')
    for i in range(0, N):
      ii = "i" + str(i)
      f.write('(assert (= ' + ii + ' (str.indexof W "' + str(W[i]) + '" 0)))\n')
      f.write('(assert (>= ' + ii + ' 0))\n')
    f.write('(check-sat)\n(get-model)\n')

for L in [100, 250, 500, 750, 1000]:
  for N in [5, 10, 15, 20]:
    w = reduce(lambda x, y: x + y, [randchar() for _ in range(0, L)])
    indexes = [0, 0]
    while (len(indexes) > len(set(indexes))):
      indexes = sorted([randint(1, L - 1) for _ in range(0, N - 1)])
    strings = [w[0 : indexes[0]]]
    indexes = indexes + [L]
    strings += [w[indexes[i] : indexes[i + 1]] for i in range(0, N - 1)]
    assert len(strings) == N and sum(len(s) for s in strings) == L
    for i in range(0, N - 1):
      for j in range(i + 1, N):
        if randint(0, 1):
          # Force an overlap between string[i] and string[j].
          choice = randint(0, 2)
          li = len(strings[i])
          lj = len(strings[j])
          assert li > 0 and lj > 0
          if (choice == 0):
            if li < lj:
              if strings[j].find(strings[i]) == -1:
                # sub(i, j)
                n = randint(0, lj - li)
                strings[j] = strings[j][:n] + strings[i] + strings[j][n + li:]
            elif (strings[i].find(strings[j]) == -1):
              # sub(j, i)
              n = randint(0, li - lj)
              strings[i] = strings[i][:n] + strings[j] + strings[i][n + lj:]
          elif (choice == 1):
            # i before j
            n = randint(1, min(li, lj))
            strings[j] = strings[i][-n:] + strings[j][n:]
          else:
            # j before i
            n = randint(1, min(li, lj))
            strings[j] = strings[i][-n:] + strings[j][n:]
    assert len(strings) == N and sum(len(s) for s in strings) == L
    # Output to dzn.
    output_dzn(L, N, strings)
    output_smt(L, N, strings)

