import csv

path = '/home/roberto/exp_cpaior_2018/results/results_pisa.csv'

reader = csv.reader(open(path), delimiter = '|')

table = {}
for row in reader:
  solv = row[0]
  inst = row[1]
  if solv == 'G-Strings':
    solv += inst[:-4]
  inst = inst[:inst.find('.')]
  if row[2] == 'unsat':
    inst += '*'
  if not inst in table:
    table[inst] = []
  table[inst] += [float(row[3])]
  
print 'instance & z3str3 & cvc4 & G-Strings500 & G-Strings1000 & G-Strings5000 & G-Strings10000 \\\\'
for x in sorted(table.items()):
  print x[0],'&',
  for y in x[1][:-1]:
    print y,'&',
  print x[1][-1],'\\\\'
