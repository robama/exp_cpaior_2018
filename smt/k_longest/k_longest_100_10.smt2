(declare-fun L () Int)
(declare-fun N () Int)
(declare-fun selected_0 () String)
(declare-fun count_0_0 () Int)
(declare-fun count_0_1 () Int)
(declare-fun count_0_2 () Int)
(declare-fun count_0_3 () Int)
(declare-fun count_0_4 () Int)
(declare-fun count_0_5 () Int)
(declare-fun count_0_6 () Int)
(declare-fun count_0_7 () Int)
(declare-fun count_0_8 () Int)
(declare-fun count_0_9 () Int)
(declare-fun selected_1 () String)
(declare-fun count_1_0 () Int)
(declare-fun count_1_1 () Int)
(declare-fun count_1_2 () Int)
(declare-fun count_1_3 () Int)
(declare-fun count_1_4 () Int)
(declare-fun count_1_5 () Int)
(declare-fun count_1_6 () Int)
(declare-fun count_1_7 () Int)
(declare-fun count_1_8 () Int)
(declare-fun count_1_9 () Int)
(declare-fun selected_2 () String)
(declare-fun count_2_0 () Int)
(declare-fun count_2_1 () Int)
(declare-fun count_2_2 () Int)
(declare-fun count_2_3 () Int)
(declare-fun count_2_4 () Int)
(declare-fun count_2_5 () Int)
(declare-fun count_2_6 () Int)
(declare-fun count_2_7 () Int)
(declare-fun count_2_8 () Int)
(declare-fun count_2_9 () Int)
(declare-fun selected_3 () String)
(declare-fun count_3_0 () Int)
(declare-fun count_3_1 () Int)
(declare-fun count_3_2 () Int)
(declare-fun count_3_3 () Int)
(declare-fun count_3_4 () Int)
(declare-fun count_3_5 () Int)
(declare-fun count_3_6 () Int)
(declare-fun count_3_7 () Int)
(declare-fun count_3_8 () Int)
(declare-fun count_3_9 () Int)
(declare-fun selected_4 () String)
(declare-fun count_4_0 () Int)
(declare-fun count_4_1 () Int)
(declare-fun count_4_2 () Int)
(declare-fun count_4_3 () Int)
(declare-fun count_4_4 () Int)
(declare-fun count_4_5 () Int)
(declare-fun count_4_6 () Int)
(declare-fun count_4_7 () Int)
(declare-fun count_4_8 () Int)
(declare-fun count_4_9 () Int)
(assert (= N 9))
(assert (ite (str.contains "@R(?L*mzS" selected_0)(= count_0_0 1)(= count_0_0 0)))
(assert (ite (str.contains "m0ViUv)FC`V`D" selected_0)(= count_0_1 1)(= count_0_1 0)))
(assert (ite (str.contains "XJm@R(?L*" selected_0)(= count_0_2 1)(= count_0_2 0)))
(assert (ite (str.contains "/em0ViUv)F9oY=V`De|xwa" selected_0)(= count_0_3 1)(= count_0_3 0)))
(assert (ite (str.contains "8eXpXJmzS3l@R(?L*" selected_0)(= count_0_4 1)(= count_0_4 0)))
(assert (ite (str.contains "@R(?L*" selected_0)(= count_0_5 1)(= count_0_5 0)))
(assert (ite (str.contains "(?L*3e" selected_0)(= count_0_6 1)(= count_0_6 0)))
(assert (ite (str.contains "(?L*D" selected_0)(= count_0_7 1)(= count_0_7 0)))
(assert (ite (str.contains "V(?L*DS9#" selected_0)(= count_0_8 1)(= count_0_8 0)))
(assert (ite (str.contains "(?L*" selected_0)(= count_0_9 1)(= count_0_9 0)))
(assert (ite (= selected_0 selected_1)(= selected_0 "") (<= (str.len selected_0)(str.len selected_1))))
(assert (ite (= selected_0 selected_2)(= selected_0 "") (<= (str.len selected_0)(str.len selected_2))))
(assert (ite (= selected_0 selected_3)(= selected_0 "") (<= (str.len selected_0)(str.len selected_3))))
(assert (ite (= selected_0 selected_4)(= selected_0 "") (<= (str.len selected_0)(str.len selected_4))))
(assert (<= N (+ count_0_0 count_0_1 count_0_2 count_0_3 count_0_4 count_0_5 count_0_6 count_0_7 count_0_8 count_0_9 )))
(assert (ite (str.contains "@R(?L*mzS" selected_1)(= count_1_0 1)(= count_1_0 0)))
(assert (ite (str.contains "m0ViUv)FC`V`D" selected_1)(= count_1_1 1)(= count_1_1 0)))
(assert (ite (str.contains "XJm@R(?L*" selected_1)(= count_1_2 1)(= count_1_2 0)))
(assert (ite (str.contains "/em0ViUv)F9oY=V`De|xwa" selected_1)(= count_1_3 1)(= count_1_3 0)))
(assert (ite (str.contains "8eXpXJmzS3l@R(?L*" selected_1)(= count_1_4 1)(= count_1_4 0)))
(assert (ite (str.contains "@R(?L*" selected_1)(= count_1_5 1)(= count_1_5 0)))
(assert (ite (str.contains "(?L*3e" selected_1)(= count_1_6 1)(= count_1_6 0)))
(assert (ite (str.contains "(?L*D" selected_1)(= count_1_7 1)(= count_1_7 0)))
(assert (ite (str.contains "V(?L*DS9#" selected_1)(= count_1_8 1)(= count_1_8 0)))
(assert (ite (str.contains "(?L*" selected_1)(= count_1_9 1)(= count_1_9 0)))
(assert (ite (= selected_1 selected_2)(= selected_1 "") (<= (str.len selected_1)(str.len selected_2))))
(assert (ite (= selected_1 selected_3)(= selected_1 "") (<= (str.len selected_1)(str.len selected_3))))
(assert (ite (= selected_1 selected_4)(= selected_1 "") (<= (str.len selected_1)(str.len selected_4))))
(assert (<= N (+ count_1_0 count_1_1 count_1_2 count_1_3 count_1_4 count_1_5 count_1_6 count_1_7 count_1_8 count_1_9 )))
(assert (ite (str.contains "@R(?L*mzS" selected_2)(= count_2_0 1)(= count_2_0 0)))
(assert (ite (str.contains "m0ViUv)FC`V`D" selected_2)(= count_2_1 1)(= count_2_1 0)))
(assert (ite (str.contains "XJm@R(?L*" selected_2)(= count_2_2 1)(= count_2_2 0)))
(assert (ite (str.contains "/em0ViUv)F9oY=V`De|xwa" selected_2)(= count_2_3 1)(= count_2_3 0)))
(assert (ite (str.contains "8eXpXJmzS3l@R(?L*" selected_2)(= count_2_4 1)(= count_2_4 0)))
(assert (ite (str.contains "@R(?L*" selected_2)(= count_2_5 1)(= count_2_5 0)))
(assert (ite (str.contains "(?L*3e" selected_2)(= count_2_6 1)(= count_2_6 0)))
(assert (ite (str.contains "(?L*D" selected_2)(= count_2_7 1)(= count_2_7 0)))
(assert (ite (str.contains "V(?L*DS9#" selected_2)(= count_2_8 1)(= count_2_8 0)))
(assert (ite (str.contains "(?L*" selected_2)(= count_2_9 1)(= count_2_9 0)))
(assert (ite (= selected_2 selected_3)(= selected_2 "") (<= (str.len selected_2)(str.len selected_3))))
(assert (ite (= selected_2 selected_4)(= selected_2 "") (<= (str.len selected_2)(str.len selected_4))))
(assert (<= N (+ count_2_0 count_2_1 count_2_2 count_2_3 count_2_4 count_2_5 count_2_6 count_2_7 count_2_8 count_2_9 )))
(assert (ite (str.contains "@R(?L*mzS" selected_3)(= count_3_0 1)(= count_3_0 0)))
(assert (ite (str.contains "m0ViUv)FC`V`D" selected_3)(= count_3_1 1)(= count_3_1 0)))
(assert (ite (str.contains "XJm@R(?L*" selected_3)(= count_3_2 1)(= count_3_2 0)))
(assert (ite (str.contains "/em0ViUv)F9oY=V`De|xwa" selected_3)(= count_3_3 1)(= count_3_3 0)))
(assert (ite (str.contains "8eXpXJmzS3l@R(?L*" selected_3)(= count_3_4 1)(= count_3_4 0)))
(assert (ite (str.contains "@R(?L*" selected_3)(= count_3_5 1)(= count_3_5 0)))
(assert (ite (str.contains "(?L*3e" selected_3)(= count_3_6 1)(= count_3_6 0)))
(assert (ite (str.contains "(?L*D" selected_3)(= count_3_7 1)(= count_3_7 0)))
(assert (ite (str.contains "V(?L*DS9#" selected_3)(= count_3_8 1)(= count_3_8 0)))
(assert (ite (str.contains "(?L*" selected_3)(= count_3_9 1)(= count_3_9 0)))
(assert (ite (= selected_3 selected_4)(= selected_3 "") (<= (str.len selected_3)(str.len selected_4))))
(assert (<= N (+ count_3_0 count_3_1 count_3_2 count_3_3 count_3_4 count_3_5 count_3_6 count_3_7 count_3_8 count_3_9 )))
(assert (ite (str.contains "@R(?L*mzS" selected_4)(= count_4_0 1)(= count_4_0 0)))
(assert (ite (str.contains "m0ViUv)FC`V`D" selected_4)(= count_4_1 1)(= count_4_1 0)))
(assert (ite (str.contains "XJm@R(?L*" selected_4)(= count_4_2 1)(= count_4_2 0)))
(assert (ite (str.contains "/em0ViUv)F9oY=V`De|xwa" selected_4)(= count_4_3 1)(= count_4_3 0)))
(assert (ite (str.contains "8eXpXJmzS3l@R(?L*" selected_4)(= count_4_4 1)(= count_4_4 0)))
(assert (ite (str.contains "@R(?L*" selected_4)(= count_4_5 1)(= count_4_5 0)))
(assert (ite (str.contains "(?L*3e" selected_4)(= count_4_6 1)(= count_4_6 0)))
(assert (ite (str.contains "(?L*D" selected_4)(= count_4_7 1)(= count_4_7 0)))
(assert (ite (str.contains "V(?L*DS9#" selected_4)(= count_4_8 1)(= count_4_8 0)))
(assert (ite (str.contains "(?L*" selected_4)(= count_4_9 1)(= count_4_9 0)))
(assert (<= N (+ count_4_0 count_4_1 count_4_2 count_4_3 count_4_4 count_4_5 count_4_6 count_4_7 count_4_8 count_4_9 )))
(assert (= L (+(str.len selected_0)(str.len selected_1)(str.len selected_2)(str.len selected_3)(str.len selected_4))))
(check-sat)
(get-model)
